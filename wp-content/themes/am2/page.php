<?php get_header(); ?>

<!-- start:content -->
<div id="content">
	<?php $bg_image = get_field( 'pozadinska_slika', $page->ID);  ?>
    <!-- start:main 	-->
    <main id="main" class="main clearfix parallax-background" data-parallax="scroll" data-image-src="<?php echo $bg_image['url'] ?>">
    	<!-- start:header title-->
		<?php 
	        $header_title = array();         
	        $header_title_image = get_field( 'main_header_image', 'options' );
	        $header_title['header_image'] = $header_title_image['url'];	        
	        if($post->post_parent) {
				$header_title['header_title'] =  get_the_title($post->post_parent); 
			}
			else {
				$header_title['header_title'] =  get_the_title($post->ID);
			}  
	        Timber::render('header-title.twig', $header_title);            
	    ?>
	    <!-- end:header title-->

 		
				<?php the_content(); ?> 
    </main>
    <!-- end:main -->

</div>
<!-- end:content -->
<script>
	$( document ).ready( function() {
	    am2.main.loadScript( 'parallax', am2.templateDirectory + '/assets/js/parallax.min.js', function() {
	        $('.parallax-background').parallax();
	    });
	});
</script>
<?php get_footer(); ?>