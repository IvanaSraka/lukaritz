var am2 = window.am2 || {};

(function ($) {
    'use strict';

    am2.main.init = function () {
        am2.main.stickyHeader();
        am2.main.mobileMenu();
    };

    am2.main.executeFunctionsQueue = function ( key, queue_array ) {
        // Remove and execute all items in the array
        while ( queue_array[key].length > 0) {
            ( queue_array[key].shift() )();
        }
    }

    am2.main.loadScript = function( id, url, callback ) {  

        // If external asset is already in DOM and loaded, we can execute the callback immediately
        if(typeof am2.assetsLoaded == 'undefined') {
            am2.assetsLoaded = [];
        }
        if(am2.assetsLoaded[id] === true) {
            callback();
            return;
        }
        
        if(typeof am2.functionsQueue == 'undefined') {
            am2.functionsQueue = [];
        }
        if(typeof am2.functionsQueue[id] == 'undefined') {
            am2.functionsQueue[id] = [];
        }
        am2.functionsQueue[id].push( callback );

        //Check if script exists. If not, create it, if yes, we have already added it in the queue and we're all
        var assetExists = document.getElementById( id );
        if( null == assetExists ) {
            // Adding the script tag to the head as suggested before
            var body = document.getElementsByTagName( 'body' )[0];
            var script = document.createElement( 'script' );
            script.type = 'text/javascript';
            script.src = url;
            script.id = id;

            // Then bind the event to the callback function.
            // There are several events for cross browser compatibility.
            script.onload = function() {
                am2.assetsLoaded[id] = true;
                am2.main.executeFunctionsQueue( id, am2.functionsQueue );
            }
            script.onreadystatechange = function() {
                am2.assetsLoaded[id] = true;
                am2.main.executeFunctionsQueue( id, am2.functionsQueue );
            }

            // Fire the loading
            body.appendChild( script );
        } 

    };

    am2.main.stickyHeader = function() { 
        
        function stickyHeader() {
            var header = $( '#header' );
         //   $( '#content' ).css( "margin-top", header.outerHeight() );

          /*  if ( $( window ).scrollTop() > 1 ) {
                header.addClass( 'sticky' ); 
            } else {
                header.removeClass( 'sticky' );
                $( '#content' ).css( "margin-top", "180px" );
            } */

            if ( $( window ).scrollTop() > 1 ) {
                header.addClass( 'sticky' );
            } else {
                header.removeClass( 'sticky' );
            }
        }

        $( window ).load( function() {
            stickyHeader();
        });

        $( window ).scroll( function() {
            stickyHeader();
        });

    };

    am2.main.mobileMenu = function() {

        var respMenu = $( '.resp-menu' );
        var button   = $( '.js-resp-menu-toggle' );
        var body     = $( 'body' );

        button.click(function(e) {
            body.toggleClass('menu-open');
        });

        /* Sliding side menu, close on swipe */
        if ($().swipe) {
            respMenu.swipe({
                swipeRight: function(event, direction, distance, duration, fingerCount) {
                    body.removeClass('menu-open');
                },
                threshold: 20,
                excludedElements: ""
            });
            button.swipe({
                swipeLeft: function(event, direction, distance, duration, fingerCount) {
                    body.addClass('menu-open');
                },
                threshold: 20,
                excludedElements: ""
            });
        }

        /* Sliding side menu submenu */
        respMenu.find('.menu-item > a').click(function(e) {
            $('.menu-item').not(this).removeClass('activemenu');
            
             if($(this).parent().hasClass('activemenu')){
                $('.menu-item').removeClass('activemenu');
              }
            $(this).parent().toggleClass('activemenu');
 
            if ($(this).attr('href') == "#") {
                e.preventDefault();
                $('.sub-menu').not($(this).siblings()).slideUp(300);
                $(this).siblings('.sub-menu').slideToggle(300); 
            }
        });

        /* Adding submenu arrows to the responsive navigation 
        respMenu.find('.sub-menu').each(function() {
            $(this).parent().append('<a class="submenu-button" href="javascript:void(0)"><svg class="arrow-down" width="15px" height="8px"><polygon fill="#fff" points="15.002,0 7.501,7.501 0,0 "></polygon></svg></a>');
        });*/

        $('.sub-menu').each(function() {
            $(this).parent().append('<svg class="submenu-button" version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 28" style="enable-background:new 0 0 32 28;" xml:space="preserve"><path style="fill-rule:evenodd;clip-rule:evenodd;" d="M16,28L32,0H0L16,28"/></svg>');
        });

  

    };

return am2.main.init();

}($));
