var am2 = window.am2 || {};

(function ($) {
    'use strict';

    am2.main.initFunctions = function () {
        am2.main.onClickEvents();
    };

    am2.main.onClickEvents = function ( args ) {
        $( window ).load( function() {
            //put various on click functionalities here
        });
    };

    am2.main.accordion = function ( args ) {

        var defaults = {
            "blockId": "am2-accordion-1",
            "animationTime": 300,
        };

        var options = $.extend( defaults, args );

        // AM2 Accordion
        $( '#' + options.blockId + '' ).on( 'click', '.am2-accordion__panel', function () {
            if ( $( this ).hasClass( 'am2-accordion__panel--current' ) ) {
                $( this ).removeClass( 'am2-accordion__panel--current' ).find( '.am2-accordion__panel-content' ).slideUp( options.animationTime );
            } else {
                var currentAccordionPanel = $( '#' + options.blockId + ' .am2-accordion__panel--current' );
                if ( currentAccordionPanel.length > 0 ) {
                    currentAccordionPanel.removeClass( 'am2-accordion__panel--current' ).find( '.am2-accordion__panel-content' ).slideUp( options.animationTime );
                }
                $( this ).addClass( 'am2-accordion__panel--current' ).find( '.am2-accordion__panel-content' ).slideDown( options.animationTime );
            }
        });

    };

    am2.main.tabs = function( args ) {

        var defaults = {
			"animationTime": 300
        };

        var options = $.extend( defaults, args );

        // Set first tab active
        $( '#am2-tabs-' + options.blockId + ' .am2-tabs__title:first-child' ).addClass( 'am2-tabs__title--current' );
        $( '#am2-tabs-' + options.blockId + ' .am2-tabs__panel:first-child' ).addClass( 'am2-tabs__panel--current' );

        // Get current tab from URL
        var hash = $.trim( window.location.hash );
        var $blockId = hash.split( '-' );
        if ( hash ) {
            var $index = $( '#am2-tabs-' + $blockId[1] + ' .am2-tabs__link[href=' + hash + ']' ).parent().index();
            $( '#am2-tabs-' + $blockId[1] + ' .am2-tabs__title' ).removeClass( 'am2-tabs__title--current' );
            $( '#am2-tabs-' + $blockId[1] + ' .am2-tabs__panel' ).removeClass( 'am2-tabs__panel--current' );
            $( '#am2-tabs-' + $blockId[1] + ' .am2-tabs__link[href=' + hash + ']' ).parent().addClass( 'am2-tabs__title--current' );
            $( '#am2-tabs-' + $blockId[1] + ' .am2-tabs__panel' ).eq( $index ).addClass( 'am2-tabs__panel--current' );
        }
        
        // AM2 Tabs
        $( '#am2-tabs-' + options.blockId + '' ).on( 'click', '.am2-tabs__title', function () {
            var $that = $( this );
            if ( ! $( this ).hasClass( 'am2-tabs__title--current' ) ) {
                $( '#am2-tabs-' + options.blockId + ' .am2-tabs__title' ).removeClass( 'am2-tabs__title--current' );
                $( '#am2-tabs-' + options.blockId + ' .am2-tabs__panel' ).fadeOut( options.animationTime ).promise().done( function() {
                    $( '#am2-tabs-' + options.blockId + ' .am2-tabs__panel' ).removeClass( 'am2-tabs__panel--current' );
                    $( '#am2-tabs-' + options.blockId + ' .am2-tabs__panels' )
                    .children( '.am2-tabs__panel' )
                    .eq( $that.index() )
                    .fadeIn( options.animationTime )
                    .addClass( 'am2-tabs__panel--current' );
                } );

                $( this ).addClass( 'am2-tabs__title--current' );
            }
        });  
    };

    am2.main.slickSlider = function ( args ) {

        var defaults = {};

        var options = $.extend( defaults, args );

        $( window ).load( function() {
            $( '#' + options.blockId + '' ).slick( options.options );
        });
        
    };

    am2.main.socialShare = function ( args ) {

        var twitterShare = document.querySelector('[data-js="twitter-share"]');

        twitterShare.onclick = function(e) {
          e.preventDefault();
          var twitterWindow = window.open('https://twitter.com/share?url=' + document.URL, 'twitter-popup', 'height=350,width=600');
          if(twitterWindow.focus) { twitterWindow.focus(); }
            return false;
          }

        var facebookShare = document.querySelector('[data-js="facebook-share"]');

        facebookShare.onclick = function(e) {
          e.preventDefault();
          var facebookWindow = window.open('https://www.facebook.com/sharer/sharer.php?u=' + document.URL, 'facebook-popup', 'height=350,width=600');
          if(facebookWindow.focus) { facebookWindow.focus(); }
            return false;
        }
        
    };

    am2.main.appearAnimations = function() {
        $( window ).on( 'load', function() {
            ( function( $ ) {
                $( '[data-appear-effect]' ).each( function() {
                    $( this ).waypoint( function( direction ) {             
                        var $element = $( this.element ),
                        options  = { 
                            effect: $element.data( 'appear-effect' ),
                            delay:  $element.data( 'appear-delay' )
                        };
        
                        $element.addClass( options.effect );
        
                        if ( options.delay > 1 ) {
                            $element.css( 'animation-delay', options.delay + 'ms' );
                        }   
        
                        setTimeout( function() {
                            $element.addClass( 'animated' );
                        }, options.delay );         
                    }, {
                        offset: '90%'
                    })
                });
            }).apply( this, [jQuery] );
        }); 
    }
 
    am2.main.scrollAnimations = function() {   
        $(window).scroll(function (event) {
            $(".slika-tekst__animation").stop().animate({"top":  140 - ($(document).scrollTop() / 7) + "px"});
            $(".slika-tekst__animation--sec").stop().animate({"top":  520 - ($(document).scrollTop() / 7) + "px"});                     

        });
    };

    am2.main.googleMap = function ( args ) {  
        var pathArray = 'https://myzonedev.com/lukaritz/'; 

        var defaults = {
            "zoom": 16,
            "scrollwheel": false,
			"draggable": false
        };

        var options = $.extend( defaults, args );

        $( window ).load( function() {

            var map = new google.maps.Map( document.getElementById( options.blockId ), {
                zoom: options.zoom,
                scrollwheel: options.scrollwheel,
                draggable: options.draggable,
                center: new google.maps.LatLng( options.lat, options.lng ),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [ {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fbe159"
                    }
                ]
            },{
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#81ddae"
                    }
                ]
            }]
            });

            var infowindow = new google.maps.InfoWindow({ maxWidth: 350 });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng( options.lat, options.lng ),
                map: map, 
                icon: {
                    url: pathArray + "wp-content/themes/am2/assets/images/pin.png"
                },
            });

            google.maps.event.addListener( marker, 'click', ( function( marker ) {
                return function() {
                    infowindow.setContent('<p class="am2-map__info">' + options.address + '</p>');
                    infowindow.open( map, marker );
                }
            })( marker ));

        });

    };

    return am2.main.initFunctions();

}($));
