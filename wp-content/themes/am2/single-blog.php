<?php get_header(); ?>

<!-- start:content -->
<!-- start:content -->
<div id="content">

    <!-- start:main     -->
    <main id="main" class="main blog-single clearfix">
        <!-- start:header title-->
        <?php 
            $header_title = array();         
            $header_title_image = get_field( 'main_header_image', 'options' );
            $header_title['header_image'] = $header_title_image['url'];      
            $header_title['header_title'] =  __( 'Blog', 'am2' );
            
            Timber::render('header-title.twig', $header_title);            
        ?>
        <!-- end:header title-->

		<div class="wrapper layout">
            <div class="container clearfix">
                <div class="col-23 blog-single__content">
                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>                
                        <div class="blog-single__top global-text"><?php _e( 'Current', 'am2' ); ?></div>
                            <h3 class="blog-single__title"><?php the_title(); ?></h3>
                            <time class="blog-single__date"><?php _e( 'Posted', 'am2' ); ?>: <?php echo get_the_time('d. F Y.', $post->ID); ?></time>
                            <div class="blog-single__img">
                            <?php  
                            if ( has_post_thumbnail() ) { ?>
                                <figure class="blog-single__image" style="background-image:url( <?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?>);"> 
                                    <meta itemprop="image" content="<?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?>">  
                                </figure>                                  
                            <?php }  ?>
                            <!-- start:blog social-->
                            <ul class="blog-single__social">
                                <li class="social-share">
                                    <a class="social-share__icon" rel="nofollow" href="http://www.facebook.com/" onclick="popUp=window.open(
'http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>&amp;title=<?php the_title(); ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400'); popUp.focus(); return false">
                                        <svg>
                                            <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#social--facebook"></use>
                                        </svg>
                                    </a>
                                </li>   
                                <li class="social-share">
                                    <a class="social-share__icon" rel="nofollow" href="http://www.plus.google.com/" onclick="popUp=window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400'); popUp.focus(); return false">
                                    <svg>
                                        <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#social--googleplus"></use>
                                    </svg>
                                    </a>
                                </li> 
                                <li class="social-share">
                                    <a class="social-share__icon" rel="nofollow" href="http://twitter.com/" onclick="popUp=window.open(
                                    'http://twitter.com/intent/tweet?status=<?php the_title(); ?>+<?php the_permalink(); ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400');
                                    popUp.focus(); return false"> 
                                    <svg>                                        
                                        <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#social--twitter"></use>
                                    </svg>
                                    </a>
                                </li>        
                                <li class="social-share">
                                    <a class="social-share__icon" rel="nofollow" href="http://pinterest.com/" onclick="popUp=window.open(
'http://pinterest.com/pin/create/bookmarklet/?media=<?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?>&amp;url=<?php the_permalink(); ?>/&amp;is_video=false&amp;description=<?php the_title(); ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400');
popUp.focus(); return false">
                                    <svg >
                                        <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#social--pinterest"></use>
                                    </svg>
                                    </a>
                                </li>   
                                <li class="social-share">
                                    <a class="social-share__icon" rel="nofollow" href="http://www.linkedin.com/" onclick="popUp=window.open(
'http://www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400'); popUp.focus(); return false">
                                    <svg>
                                        <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#social--linkedin"></use>
                                    </svg>
                                    </a>
                                </li>  
                                <li class="social-share">
                                    <a class="social-share__icon" href="mailto:?subject=<?php the_title(); ?>&amp;body=Preporuka 'Održana informativna radionica HAMAG-BICRO-a u Belom Manastiru' at <?php the_permalink(); ?>"> 
                                    <svg>
                                        <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#icon--email"></use>
                                    </svg>
                                    </a>
                                </li>            
                            </ul>
                            <!-- end:blog social-->
                            </div>
                        <?php the_content(); ?>
                    <?php endwhile; endif;?> 
 
                </div>

                <aside class="col-13 no-margin blog-single__sidebar">
                    <?php include(locate_template(array('sidebar.php')));  ?>
                </aside>

            </div>
        </div>

	</main>
	<!-- end:main -->
 
</div>
<!-- end:content -->

<?php get_footer(); ?> 