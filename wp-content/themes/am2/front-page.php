<?php
/* Template Name: Homepage */
?>
<?php get_header(); ?>

<!-- start:content -->
<div id="content">

    <!-- start:main -->
    <main id="main" class="main clearfix">
		<?php the_content(); ?>
    </main>
    <!-- end:main -->

<!-- end:content -->

<?php get_footer(); ?>