<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>
			<?php
				$blog_title = null;
				$seo_title  = null;
				$blog_title = get_bloginfo('name') . ' - ' . get_the_title();
				$seo_title  = get_post_meta( get_the_ID(), '_yoast_wpseo_title', true );
				if ( ! empty( $seo_title ) ) {
					if ( is_home() ) {
						echo $blog_title;
					} else {
						echo $seo_title;
					}
				} else {
					if ( is_archive() ) {
						wp_title( '' );
						echo ' - ';
					} elseif ( is_category() ) {
						wp_title( '' );
						echo ' - ';
					} elseif ( is_search() ) {
						echo 'Search term &quot;' . _wp_specialchars( $s ) . '&quot; - ';
					} elseif ( ! ( is_404() ) && ( is_single() ) || ( is_page() ) ) {
						if ( ! is_front_page() ) {
							wp_title( '' );
							echo ' - ';
						}
					} elseif ( is_404() ) {
						echo 'Not found - ';
					}
					if ( is_front_page() ) {
						bloginfo( 'name' );
					} elseif ( is_home() ) {
						echo( 'Offer - ' );
						bloginfo( 'name' );
					} else {
						bloginfo( 'name' );
					}
					if ( $paged > 1 ) {
						echo ' - Page ' . $paged;
					}
				}
			?>
		</title>

		<?php $favicon = get_field( 'favicon', 'options' ); ?>
        <?php if( ! empty( $favicon ) ) { ?>
            <link rel="icon" href="<?php echo $favicon['url']; ?>">
        <?php } ?>

		<?php wp_head(); ?>

		<?php
			$tracking_codes_head = get_field( 'tracking_codes_head', 'options' );
			if ( ! empty( $tracking_codes_head ) ) {
				echo $tracking_codes_head;
			}
		?>

	</head>

	<body <?php body_class(); ?>>

		<?php
			$tracking_codes_body = get_field( 'tracking_codes_body', 'options' );
			if ( ! empty( $tracking_codes_body ) ) {
				echo $tracking_codes_body;
			}
		?>

		<!-- start:main nav -->		
		<div class="resp-menu">

			<!-- start:language and social menu -->
			<div class="menu-top menu-top--responsive">
			<?php 
				wp_nav_menu( array(
					'container'       => 'ul', 
					'theme_location'  => 'language_menu', 
					) 
				); 
			?> 			
			<!-- end:language and social menu -->
			
				<!-- start:responsive buttons -->
				<div class="resp-buttons--close right">
					<div class="resp-menu-icon js-resp-menu-toggle" >
			            <span class="resp-menu-icon__line resp-menu-icon__line--top"></span>
			            <span class="resp-menu-icon__line resp-menu-icon__line--middle"></span>
			            <span class="resp-menu-icon__line resp-menu-icon__line--bottom"></span>
			        </div>
				</div>
				<!-- end:responsive buttons -->
			</div>
			
			<!-- start:main nav -->
			<?php
			wp_nav_menu( array(
					'container'       => 'nav',
					'container_id'    => 'resp-main-nav',
					'container_class' => 'menu menu--responsive-menu',
					'menu_class'      => 'menu clearfix',
					'theme_location'  => 'main_menu',
					'fallback_cb'	  => 'am2_fallback_menu'
				)
			);
			?>
			<!-- end:main nav -->			
		</div>
		<!-- end:main nav -->

		<!-- start:wrapper -->
		<div id="wrapper">

			<!-- start:header -->
			<header id="header" class="header header--site">
				<div class="wrapper">
					<div class="container">

						<?php 
							$site_logo = get_field( 'site_logo', 'options' ); 
							$site_inverted_logo = get_field( 'site_inverted_logo', 'options' ); 
						?>
						<div class="logo">
							<a href="<?php echo site_url(); ?>" class="logo__link">
								<?php if( !empty( $site_logo ) ) { ?>
									<figure class="logo__image">
										<meta itemprop="image" content="<?php echo $site_logo['url']; ?>">										
									</figure> 
								<?php } else { 
									bloginfo();
								} ?>
							</a>
						</div>
						<style type="text/css">
							.logo__image { background-image:url( <?php echo $site_logo['url']; ?> );  }
							.sticky .logo__image { background-image:url( <?php echo $site_inverted_logo['url']; ?> );  }
						</style>
						<!-- start:main nav -->
						<?php 
							wp_nav_menu( array(
								'container'       => 'nav',
								'container_id'    => 'main-nav',
								'container_class' => 'main-navigation',
								'menu_class'      => 'menu menu--main-menu clearfix',
								'theme_location'  => 'main_menu',
								'fallback_cb'	  => 'am2_fallback_menu' 
								) 
							); 
						?>
						<!-- end:main nav -->

						<!-- start:language and social menu -->
						<div class="menu-top menu-top--desktop">
						<?php 
							wp_nav_menu( array(
								'container'       => 'ul', 
								'theme_location'  => 'language_menu', 
								) 
							); 
						?>
						<?php echo am2_social_links(); ?>
						</div>
						<!-- end:language and social menu -->

						<!-- start:responsive buttons -->
						<div class="resp-buttons resp-buttons--open right">
							<div class="resp-menu-icon js-resp-menu-toggle" >
					            <span class="resp-menu-icon__line resp-menu-icon__line--top"></span>
					            <span class="resp-menu-icon__line resp-menu-icon__line--middle"></span>
					            <span class="resp-menu-icon__line resp-menu-icon__line--bottom"></span>
					        </div>
						</div>
						<!-- start:responsive buttons -->

					</div>
				</div>
			</header>
			<!-- end:header -->
		