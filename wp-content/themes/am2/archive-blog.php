<?php /* Template name: Blog */ ?>
<?php get_header(); ?>

<!-- start:post array -->
<?php  
    $post_type = 'blog';
    $args = array (
        'post_type' => $post_type,
        'order' => 'DESC',
        'posts_per_page' => -1, 
    );
    $query = new WP_Query($args); 
?>
<!-- end:post array -->

<!-- start:content -->
<div id="content">

    <!-- start:main     -->
    <main id="main" class="main clearfix">
        <!-- start:header title-->
        <?php 
            $header_title = array();         
            $header_title_image = get_field( 'main_header_image', 'options' );
            $header_title['header_image'] = $header_title_image['url'];         
            $header_title['header_title'] =  __( 'Blog archive', 'am2' );
            Timber::render('header-title.twig', $header_title);            
        ?>
        <!-- end:header title-->
        <div class="wrapper blog-archive">
            <div class="container">
            <?php 
                if( $query->have_posts() ) {  
                    while ($query->have_posts()) {  $query->the_post(); ?>
                    <div class="blog-list clearfix">
                        <?php  
                            if ( has_post_thumbnail() ) { ?>
                                <figure class="blog-list__image" style="background-image:url( <?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?>);"> 
                                    <meta itemprop="image" content="<?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?>">  
                                </figure>                                  
                        <?php }  ?> 

                        <div class="blog-list__content">  
                            <div class="blog-list__content-text">
                                <div class="blog-list__date"><?php echo get_the_date('d. F Y.', $query->ID ); ?></div>                         
                                <h2 class="blog-list__title">
                                    <?php the_title(); ?>
                                </h2> 
                                <?php the_excerpt(); ?> 
                            </div>
                            <a href="<?php the_permalink(); ?>" class="button button--primary button--v1">                       
                            <?php _e( 'more', 'am2' ); ?>          
                            </a>                                        
                        </div>
                         
                    </div>       
            <?php   } 
                } ?>   
            </div>
        </div>   
    </main> 
    <!-- end:main -->

</div>
<!-- end:content --> 
<?php get_footer(); ?>