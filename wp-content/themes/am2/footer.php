			<!-- start:footer -->
			<footer id="footer" class="footer footer--site">
				<div class="wrapper">
					<div class="container clearfix">
					
						<div class="copyright">
							<?php echo get_field( 'copyright', 'options' ); ?>
						</div>

						<div class="social">
							<?php echo am2_social_links(); ?>
						</div>

					</div>
				</div>
			</footer>
			<!-- end:footer -->
		</div>
		<!-- end:wrapper -->
		<?php if( ICL_LANGUAGE_CODE === "en" ) { ?>
			<script>
				$( window ).on( 'load', function() { 
		            $(".anketa_view_res").append("Total Voters: "); 
		            $("#button-vote").val(" Vote  ");
		            $("#voted_list").append("Vote");
		            $(".total_voters_text").append("View results"); 
		        });
			</script>
		<?php }else { ?>
			<script>
				$( window ).on( 'load', function() {  
					$(".total_voters_text").append("Ukupni glasovi: "); 
		            $(".anketa_view_res").append("Pogledaj rezultate");
		            $("#button-vote").val(" Glasaj  ");
		            $("#voted_list").append("Glasaj");
		        });
			</script>
		<?php } ?>
		<?php wp_footer(); ?> 
		
		<?php
			$tracking_codes_footer = get_field( 'tracking_codes_footer', 'options' );
			if ( ! empty( $tracking_codes_footer ) ) {
				echo $tracking_codes_footer;
			}
		?>
		<script>
			$( document ).ready( function() {
				svg4everybody(); 
			});
		</script>
		
	</body>
</html>