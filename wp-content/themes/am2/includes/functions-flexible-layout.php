<?php
/* Insert function description/documentation here TODAY */
/*
USAGE:
	$field_name = 'flexible_layout';
	$blocks     = get_field( $field_name );
	$field_render = new AM2_Flexible_Layout($blocks);
	$output = $field_render->render();
	echo $output;
*/

function am2_handle_settings( $properties ) {
	if ( empty( $properties ) ) {
		return null;
	}

	$style = '';

	foreach ( $properties as $group ):
		foreach ( $group as $prop => $att ):
			if ( empty( $att ) && $att != '0' ) {
				continue;
			}
			if ( $prop == 'acf_fc_layout' ) {
				continue;
			}
			if ( $prop == 'background_type' ) {
				continue;
			}
			if ( $prop == 'background-image' ) {
				$style .= $prop . ':url(' . $att . ');';
				continue;
			}
			if ( $prop == 'color' ) {
				$style .= 'color:' . $att . ' !important;';
				continue;
			}
			$px_fields = array( 'padding', 'margin', 'width', 'height', 'line-height', 'font-size' );
			foreach ( $px_fields as $px_field ):
				if ( strpos( $prop, $px_field ) !== false ) {
					$style .= $prop . ':' . $att . 'px;';
					continue 2;
				}
			endforeach;
			if ( strpos( $prop, '%' ) ) {
				$style .= str_replace( "%", "", $prop ) . ':' . $att . '%;';
				continue;
			}
			$style .= $prop . ':' . $att . ';';

		endforeach;
	endforeach;

	if ( ! empty( $style ) ) {
		$style = 'style="' . $style . '"';
	}

	return $style;
}

// Register the autoloader.
include('flexible-layout/autoloader.php');
$autoload = new AM2_Autoloader();
$autoload->register();
// Runs this plugin.

class AM2_Flexible_Layout {

	function __construct($field, $is_flexible = true, $field_name = '') {
		$this->field = $field;
		$this->is_flexible = $is_flexible;
		$this->field_name = $field_name;

		//AM2_Flexible_Layout_Autoloader
		//$this->autoload();
	}

	public function render() {
		$output = '';

		$wrapper_open = '<div class="flexible-block">';
		$wrapper_close = '</div>';

		/* Note: fix block position and carousel? (slider? or gallery?) logic */
		global $block_position;
		if ( empty( $block_position ) ) {
			$block_position = 0;
		}

		$blocks = $this->field;

		if ( empty( $blocks ) ) {
			return null;
		}

		/* SETTINGS
			naming convention:
			#block_#setting_name
			general_#setting_name
		*/

		/* this options should be revised */
		$settings = array(
			'articles_excerpt_length' => 450,
			'articles_more_string'    => '[...]',
			'gallery_dummy_width'     => 220,
			'gallery_dummy_height'    => 220
		);

		if($this->is_flexible):

			foreach ( $blocks as $block ):
				$block_position ++;
				$context                   = array();
				$context['block_position'] = $block_position;

				$class_name = 'AM2_Flexible_Layout_Render_'.$block['acf_fc_layout'];
				if(class_exists($class_name)) {
					$handle_block = new $class_name($block);
					$output .= $wrapper_open;
					$output .= $handle_block->render();
					$output .= $wrapper_close;
				}

			endforeach;

		elseif ( !empty($blocks[$this->field_name]) ):

			$blocks[$this->field_name] = $blocks;
			$class_name = 'AM2_Flexible_Layout_Render_'.$this->field_name;
			if(class_exists($class_name)) {
				$handle_block = new $class_name($blocks);
				$output .= $wrapper_open;
				$output .= $handle_block->render();
				$output .= $wrapper_close;
			}

		endif;

		return $output;
	}

	/*public function autoload() {
		echo dirname(__FILE__);
		print_r($_SERVER);
		print_r(glob("wp-content/themes/stranica/includes/flexible-layout/*"));

		foreach(glob(get_template_directory().'flexible-layout/*.php') as $file) {
			echo $file;
		    include_once $file;
		}
	}*/

}

class AM2_Flexible_Layout_Render_row {

	function __construct($field) {
		$this->field = $field;
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;

		global $master_columns_value;
		$master_columns_value = 0;
		$layout_type          = $block['layout'];

		if ( ! empty( $block['css_classes'] ) ) {
			$context['css_classes'] = $block['css_classes'];
		}

		if ( empty( $layout_type ) ) {
			$layout_type = 'wide';
		}

		if(empty($block['context'])) {
			$block['context'] = 'white';
		}

		if(empty($block['padding'])) {
			$block['padding'] = 'large';
		}

		$field_render = new AM2_Flexible_Layout($block['columns']);
		$context['context'] 	   = $block['context'];
		$context['padding'] 	   = $block['padding'];
		$context['section_title']  = $block['section_title'];
		$context['content_blocks'] = $field_render->render();
		$context['styles']         = am2_handle_settings( $block['properties'] );
		$context['block_position'] = $block_position;

		$output .= Timber::compile( 'layout-' . $layout_type . '.twig', $context );

		return $output;

	}
}


class AM2_Flexible_Layout_Render_columns {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'columns';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;

		global $master_columns_value;
		$column_type = 'col-' . $block['column_type'];

		$no_margin = '';
		if ( $block['column_type'] != 1 ) {

			$col_split            = str_split( $block['column_type'] );
			$col_value            = ( $col_split[0] / $col_split[1] );
			$master_columns_value = $master_columns_value + $col_value;
			if ( $master_columns_value == 1 ) {
				$master_columns_value = 0;
				$no_margin .= ' no-margin';
			}
			if ( $master_columns_value > 1 ) {
				$master_columns_value = $col_value;
			}
		}

		$context['column_type']    = $column_type . $no_margin;

		$field_render = new AM2_Flexible_Layout($block['content']);
		$context['content_blocks'] = $field_render->render();

		$output .= Timber::compile( 'content-column.twig', $context );

		return $output;

	}
}

/*
 * WYSIWYG
 */
class AM2_Flexible_Layout_Render_wysiwyg {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'wysiwyg';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['wysiwyg'] ) ) {
			return;
		}

		$context['content'] = $block['wysiwyg'];
		$context['sidebar'] = $block['sidebar'];
		$context['dokument'] = $block['dokument'];
		$context['main_title'] = $block['main_title'];
		$context['title_name'] = get_the_title();


		$nr_columns = count($context['content']);

		/*if ( 1 == $nr_columns ) {
			$context['column'] = 'col-1';
		} elseif ( 1 < $context['content'] ) {
			$context['column'] = 'col-1' . $nr_columns;
		}*/

		$output .= Timber::compile( 'wysiwyg.twig', $context );
		return $output;

	}
}

/*
 * IMAGE
 */
class AM2_Flexible_Layout_Render_image {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'image';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		};

		if ( empty( $block['image'] ) ) {
			return;
		}

		$context['image'] = $block['image'];

		$output .= Timber::compile( 'image.twig', $context );
		return $output;

	}
}

/*
 * AKTUALNO
 */
class AM2_Flexible_Layout_Render_aktualno {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'aktualno';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		};

		$context['posts_data'] = '';
		$args = array( 'post_type' => 'blog', 'posts_per_page' => '6',  'post_status' => 'publish', 'suppress_filters'  => false );
		$posts_data = get_posts($args);

		$p = 0;
		foreach ($posts_data as $post_data) { 
			$context['posts_data'][$p]['title'] = $post_data->post_title; 
			$old_date = $post_data->post_date;
			$context['posts_data'][$p]['date'] = get_the_date('d. F Y.', $post_data->ID );
			$context['posts_data'][$p]['link'] = get_permalink($post_data->ID); 
			$image_id = get_post_thumbnail_id( $post_data->ID );
			$context['posts_data'][$p]['image'] = wp_get_attachment_image_src( $image_id, 'thumb' );

			$p ++;
		} 
		
 	    $context['blog_title'] = $block['blog_title'];
 		$context['aktualno_title'] = __( 'Current', 'am2' );
 		$context['arhiva_title'] = __( 'Actuality archive', 'am2' );
 		$context['arhiva_link'] = site_url().'/blog';
		$context['pozadinska_boja'] = $block['pozadinska_boja'];
		$context['pozadinska_ikonica'] = $block['pozadinska_ikonica'];

		$output .= Timber::compile( 'aktualno.twig', $context );
		return $output;
	}
}


 
/*
 * anketa
 */
class AM2_Flexible_Layout_Render_anketa {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'anketa';  
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

	 

		$context['anketa_id'] =$block['anketa_id']; 
 		$context['anketa_title'] = __( 'Pull', 'am2' ); 
		$context['pozadinska_boja'] = $block['pozadinska_boja'];
		$context['pozadinska_ikonica'] = $block['pozadinska_ikonica'];

		$output .= Timber::compile( 'anketa.twig', $context );
		return $output;

	}
}

/*
 * ZAPOSLENICI
*/ 
class AM2_Flexible_Layout_Render_zaposlenici {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'zaposlenici';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		};

		if ( empty( $block['zaposlenici'] ) ) {
			return;
		}

		$context['naslov'] = $block['naslov'];
		$context['zaposlenici'] = $block['zaposlenici'];

		$output .= Timber::compile( 'zaposlenici.twig', $context );
		return $output;
	}
}

/*
 * VRIJEDNOSTI
 */
class AM2_Flexible_Layout_Render_vrijednosti {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'vrijednosti';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		};

		if ( empty( $block['vrijednosti'] ) ) {
			return;
		}

		$context['naslov'] = $block['naslov'];
		$context['vrijednosti'] = $block['vrijednosti'];

		$output .= Timber::compile( 'vrijednosti.twig', $context );
		return $output;
	}
}

/*
 * CILJEVI CENTRA
 */
class AM2_Flexible_Layout_Render_ciljevi_centra {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'ciljevi_centra';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		};

		if ( empty( $block['ciljevi_centra'] ) ) {
			return;
		}

		$context['naslov'] = $block['naslov'];
		$context['ciljevi_centra'] = $block['ciljevi_centra'];

		$output .= Timber::compile( 'ciljevi-centra.twig', $context );
		return $output;
	}
}

/*
 * SLIDER
 */
class AM2_Flexible_Layout_Render_slider {
	
	function __construct($field) { 
		$this->field = $field;	
		$this->field_name = 'slider';
	}

	function render() {

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['slider'] ) ) {
			return '';
		}
		$context['slider']   = $block['slider'];
		$slider_image_width  = 1400;
		$slider_image_height = 450;
		$i                   = 0;

		foreach ( $context['slider'] as $slide ) {

			$am2_image_args = array(
				'dummy_width'  => $slider_image_width,
				'dummy_height' => $slider_image_height,
				'image_id'     => $slide['slide_image']['id'],
				'image_size'   => 'slider',
				'return'       => 'html'
			);

			$context['slider'][ $i ]['image_html'] = am2_image( $am2_image_args );

			$i ++;
		}
		$context['slider_show_prev_and_next_arrows'] 	= $block['slider_show_prev_and_next_arrows'];
		$context['slider_show_bullets'] 				= $block['slider_show_bullets'];
		$context['slide_duration']   					= $block['slide_duration'];
		
		if ( 1 == $context['slider_show_prev_and_next_arrows'] ) {
			$context['slider_show_prev_and_next_arrows']   = 'true';
		} else {
			$context['slider_show_prev_and_next_arrows']   = 'false';
		}

		if ( 1 == $context['slider_show_bullets'] ) {
			$context['slider_show_bullets']   = 'true';
		} else {
			$context['slider_show_bullets']   = 'false';
		}

		$context['link']   = __( 'more about us', 'am2' );
		$output .= Timber::compile( 'slider.twig', $context );
		return $output;

	}
}
/*
 * ARTICLES
 */
class AM2_Flexible_Layout_Render_articles {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'articles';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['articles'] ) ) {
			return;
		}

		$content_boxes = $block['articles'];

		$properties        = $block['properties'];
		$context['styles'] = am2_handle_settings( $properties );

		$template_version = $block['template_version'];
		if ( $template_version ) {
			$template_version = '-' . $template_version;
		}

		$context['template_version'] = $template_version;

		$nr_boxes = count( $content_boxes );
		if ( 1 == $nr_boxes ) {
			$column_class = "col-1";
		} else {
			$column_class = "col-1" . $nr_boxes;
		}

		if ( 3 < $nr_boxes ) {
			$column_class = "col-13";
		}

		if ( ! empty( $content_boxes['items_in_row'] ) ) {
			$column_class = "col-1" . $content_boxes['items_in_row'];
		}

		$context['column_class'] = $column_class;

		foreach ( $content_boxes as $content_counter => $content_box ) {

			$type     = $content_box['type'];
			$excerpt  = null;
			$image_id = null;

			if ( 'custom' == $type || '' == $type ) {
				$image         = $content_box['image'];
				$image_id      = $image['id'];
				$excerpt       = $content_box['excerpt'];
				$data['title'] = $content_box['title'];
				$data['link']  = $content_box['link'];
			} elseif ( 'page_post' == $type ) {
				$page_post     = $content_box['page_post'];
				$image_id      = get_post_thumbnail_id( $page_post->ID );
				$excerpt       = $page_post->post_excerpt;
				$data['title'] = $page_post->post_title;
				$data['link']  = get_permalink( $page_post->ID );
			}

			$args = array();

			if ( $block['template_version'] == 'vertical' ) {
				$args = array(
					'dummy_width'  => 360,
					'dummy_height' => 265,
					'image_id'     => $image_id,
					'image_size'   => 'thumb_article',
					'return'       => 'html'
				);
			}

			if ( $block['template_version'] == 'horizontal' ) {
				$args = array(
					'dummy_width'  => 550,
					'dummy_height' => 580,
					'image_id'     => $image_id,
					'image_size'   => 'article_medium',
					'return'       => 'html'
				);
			}

			$data['index'] = $content_counter + 1;

			$data['excerpt'] = am2_excerpt( 250 );
			$data['type']    = $content_box['type'];

			$data['image_html']    = am2_image( $args );
			$data['image']         = wp_get_attachment_metadata( $image_id );
			$context['articles'][] = $data;
		}

		$output .= Timber::compile( 'articles' . $template_version . '.twig', $context );
		return $output;

	}
}

/*
 * GALLERY
 */
class AM2_Flexible_Layout_Render_gallery {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'gallery';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['gallery'] ) ) {
			return;
		}

		$gallery = $block['gallery'];

		$template_version = $block['template_version'];
		if ( $template_version ) {
			$template_version = '-' . $template_version;
		} else {
			$template_version = '-thumbs';
		}

		$context['template_version'] = $template_version;

		$nr_boxes = count( $gallery );
		if ( 1 == $nr_boxes ) {
			$column_class = "col-1";
		} else {
			$column_class = "col-1" . $nr_boxes;
		}

		if ( 5 < $nr_boxes ) {
			$column_class = "col-14";
		}
		if ( 7 < $nr_boxes ) {
			$context['gal_no'] = $nr_boxes; 
			$gal_no_nwe = $nr_boxes - '7'; 
			$context['gal_no_nwe'] = $gal_no_nwe; 
		}
		if ( ! empty( $content_boxes['items_in_row'] ) ) {
			$column_class = "col-1" . $content_boxes['items_in_row'];
		}

		$context['column_class'] = $column_class;

		foreach ( $gallery as $image ) {  
			$args = array(
				'dummy_width'  => 170,
				'dummy_height' => 170,
				'image_id'     => $image['id'],
				'image_size'   => 'thumb-large',
				'return'       => 'html'
			);

			$image_final['image_html']   = am2_image( $args );
			$image_final['image_object'] = $image;
			$context['images'][]         = $image_final;
		}

			
		$context['gallery_title'] = $block['gallery_title']; 		
		$output .= Timber::compile( 'gallery' . $template_version . '.twig', $context );
		return $output;

	}
}

/*
 * TABS
 */
class AM2_Flexible_Layout_Render_tabs {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'tabs';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['tabs'] ) ) {
			return;
		}

		$tabs            = $block['tabs'];
		$context['tabs'] = $tabs;

		$output .= Timber::compile( 'tabs.twig', $context );
		return $output;

	}
}

/*
 * ACCORDION
 */
class AM2_Flexible_Layout_Render_accordion {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'accordion';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['accordion'] ) ) {
			return;
		}

		$accordion            = $block['accordion'];
		$context['accordion'] = $accordion;

		$output .= Timber::compile( 'accordion.twig', $context );
		return $output;

	}
}

/*
 * Slika + tekst
 */
class AM2_Flexible_Layout_Render_slika_tekst {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'slika_tekst';  
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['slika_tekst'] ) ) {
			return;
		}

		$slika_tekst            = $block['slika_tekst'];
		$context['slika_tekst'] = $slika_tekst;

		$output .= Timber::compile( 'slika_tekst_citat.twig', $context );
		return $output;

	}
}

/*
 * Intro text
 */
class AM2_Flexible_Layout_Render_intro_text {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'intro_text';  
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['intro_text'] ) ) {
			return;
		}

		$context['boje'] = $block['boje'];
		$context['intro_text'] = $block['intro_text'];

		$output .= Timber::compile( 'intro-text.twig', $context );
		return $output;

	}
}

/*
 * TABLE
 */
class AM2_Flexible_Layout_Render_table {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'table';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['table'] ) ) {
			return;
		}

		$table            = $block['table'];
		$context['table'] = $table;

		$output .= Timber::compile( 'table.twig', $context );
		return $output;

	}
}


/*
 * KONTAKT FORMA
 */
class AM2_Flexible_Layout_Render_kontakt_forma {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'kontakt_forma';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['kontakt_forma'] ) ) {
			return;
		}

		$context['pozadinska_boja'] = $block['pozadinska_boja'];
		$context['kontakt_forma'] = $block['kontakt_forma'];

		$output .= Timber::compile( 'kontakt-forma.twig', $context );
		return $output;

	}
}

/*
 * CODE
 */
class AM2_Flexible_Layout_Render_code {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'code';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['code'] ) ) {
			return;
		}

		$code      = $block['code'];
		$context['code'] = $code;

		$output .= Timber::compile( 'code.twig', $context );

		return $output;

	}
}


/*
 * FILES
 */
class AM2_Flexible_Layout_Render_files {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'files'; 
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name]; 
		} 
 
 
 		$context['dokumenti']   = $block['dokumenti']; 
		if($context['dokumenti']){ foreach ( $context['dokumenti'] as $dok ) {

			$files           = $dok['files']; 

			if($files){ foreach ( $files as $file ) { 
				$file_data                = array();
				$file_data['mime_type']   = $file['file']['mime_type'];
				$file_data['type']        = '';
				$file_data['icon']        = $file['icon'];
				$file_data['name']        = $file['name'];			
				$file_data['url']         = $file['file']; 
				$file_data['description'] = $file['file_description'];
				$file_data['size']        = filesize( get_attached_file( $file['file']['id'] ) );
			    $file_data['size']        = size_format( $file_data['size'], 2 );

				if ( empty( $file_data['url'] ) ) {
					$file_data['url'] = 'javascript:void()';
					
				}

				switch ( $file_data['mime_type'] ) { 
					case 'application/pdf':
						$file_data['type'] = 'pdf';
						break;
					case 'application/vnd.ms-powerpoint':
						$file_data['type'] = 'ppt';
						break;
					case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
						$file_data['type'] = 'ppt';
						break;
					case 'application/rar':
						$file_data['type'] = 'rar';
						break;
					case 'application/zip':
						$file_data['type'] = 'zip';
						break;
					case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
						$file_data['type'] = 'docx';
						break;
					case 'application/msword':
						$file_data['type'] = 'doc';
						break;
					case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
						$file_data['type'] = 'xls';
						break;
					case 'application/vnd.ms-excel';
						$file_data['type'] = 'xls';
						break;
					default:

					    //$file_data['type'] = 'NA';
					 	//$file_ext          = explode( '.', $file_data['url'] );
					 	//$file_data['ext']  = $file_ext[ count( $file_ext ) - 1 ];
						break;
				}

				$file_data_array[] = $file_data; 
				$file_data_type_array[] = $file_data['type'];
			}}

		}	

	}
  	 	 
		$context['naslov'] = $block['naslov'];
		$context['link'] = $block['links']; 
		$context['download_label'] = __( 'Download', 'Stranica' );

		$output .= Timber::compile( 'files.twig', $context );
		return $output;

	}
}

/*
 * Testimonials
 */
class AM2_Flexible_Layout_Render_testimonials {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'testimonials';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['testimonials'] ) ) {
			return;
		}

		$context['testimonials'] = $block['testimonials'];


		if(!empty($block['testimonials']['testimonials'])) {
			$context['testimonials'] = $block['testimonials']['testimonials'];
		}

		$i = 0;

		foreach ( $context['testimonials']  as $testimonial ) {

			$args = array(
				'dummy_width'  => 150,
				'dummy_height' => 150,
				'image_id'     => $testimonial['image']['id'],
				'image_size'   => 'thumbnail',
				'return'       => 'html'
			);

			$context['testimonials'][ $i ]['image_html'] = am2_image( $args);

			$i ++;

		}

		$output .= Timber::compile( 'testimonials.twig', $context );
		return $output;

	}
}

/*
 * MAP
 */
class AM2_Flexible_Layout_Render_map {

	function __construct($field) {
		$this->field = $field;
		$this->field_name = 'map';
	}

	function render() {
		$output = '';

		global $block_position;
		$block_position ++;
		$context['block_position'] = $block_position;

		$block = $this->field;
		//This is support for acf-reusable-field-groups addon
		if(!empty($this->field[$this->field_name])) {
			$block = $this->field[$this->field_name];
		}

		if ( empty( $block['map'] ) ) {
			return;
		}

		$map = $block['map'];
		$context['map'] = $map;
		$context['map']['lat'] = (float)$map['lat'];
		$context['map']['lng'] = (float)$map['lng'];
		$context['map']['api_key'] = get_field( 'google_maps_api_key', 'options' );
		$context['naslov_top'] = __( 'Contact information', 'Stranica' );
		$context['naslov'] = $block['naslov'];
		$context['pozadinska_boja'] = $block['pozadinska_boja'];
		$context['lokacija_i_podaci'] = $block['lokacija_i_podaci'];
		$context['radno_vrijeme'] = $block['radno_vrijeme'];

		if( ! empty ( $context['map']['api_key'] ) ) {
			$output .= Timber::compile( 'map.twig', $context );
		} else {
			$output .= '<p class="notification"><strong>Google Maps:</strong> Go to Admin > Options > Website Settings and enter Google Maps API Key.</p>';
		}

		return $output;

	}
}

/* Add Flexible layout to the_content()*/
add_filter( 'the_content', 'am2_flexible_content' );
function am2_flexible_content( $data ) {
	$field_name = 'flexible_content';
	$blocks = get_field( $field_name );
	$field_render = new AM2_Flexible_Layout($blocks);
	$output = $field_render->render();

	$data .= $output;

	return $data;
}

?>