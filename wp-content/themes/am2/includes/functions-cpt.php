<?php
/*
 * CPT manager
 * All CPTs ( custom post types ) are administrated here
 */
add_action( 'init', 'am2_cpt' );

function am2_cpt() {

	$labels = array(
		'name'               => _x( 'Blog', 'post type general name' ),
		'singular_name'      => _x( 'Blog', 'post type singular name' ),
		'add_new'            => _x( 'Dodaj Blog', 'Blog' ),
		'add_new_item'       => __( 'Dodaj Blog' ),
		'edit_item'          => __( 'Editiraj Blog' ),
		'new_item'           => __( 'Novo Blog' ),
		'view_item'          => __( 'Pregledaj Blog' ),
		'search_items'       => __( 'Pretraži Blog' ),
		'not_found'          => __( 'Blog nije pronađena' ),
		'not_found_in_trash' => __( 'Blog nije pronađena u smeču' ),
		'parent_item_colon'  => '',
		'show_in_nav_menus'  => true
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'query_var'          => true,
		'has_archive'        => false,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'excerpt', 'thumbnail' ),
		'taxonomies'         => array( '' )
	);

	 
	register_post_type( 'blog', $args );
	 
}

/*
 * Taxonomy manager
 * All taxonomies are administrated here
 */
function am2_taxonomy_init() {
	register_taxonomy(
		'taxonomy',
		'services',
		array(
			'label'        => __( 'Taxonomy' ),
			'rewrite'      => array( 'slug' => 'taxonomy' ),
			'capabilities' => array()
		)
	);
}
/*
add_action( 'init', 'am2_taxonomy_init' );
*/

?>