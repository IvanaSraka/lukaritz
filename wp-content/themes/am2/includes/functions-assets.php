<?php
/*
 * Assets manager
 * All assets ( styles and scripts ) are administrated here
 */

/*
 * All thumbnails are administrated here
 */
add_theme_support( 'post-thumbnails' );
add_image_size( 'slider', 1400, 450, true );
add_image_size( 'thumb-large', 320, 240, true );

/*
 * All scripts and styles are administrated here
 */
if ( ! function_exists( 'am2_enqueue_default_assets' ) ) {

	if ( ! is_admin() ) {
		add_action( 'wp_enqueue_scripts', 'am2_enqueue_default_assets' );
	}

	function am2_enqueue_default_assets() {
		global $env;

		//Main Theme CSS file
		if( 'development' === $env ) {
			wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/css/style.css', array(), time(), null );
		} else {
			$dirCSS = new DirectoryIterator( get_stylesheet_directory() . '/assets/build/' ); // Target CSS Build Path
			foreach ($dirCSS as $file) {

				if ( pathinfo($file, PATHINFO_EXTENSION) === 'css' ) {
					$fullName = basename($file);
					$name = substr(basename($fullName), 0, strpos(basename($fullName), '.'));
					$deps = array('style');
					$media = 'all';

					wp_enqueue_style( $name, get_stylesheet_directory_uri() . '/assets/build/' . $fullName , array(), null );
				}
			}
		}
		
		//Use different jQuery version
		wp_deregister_script( 'jquery' );

		wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', null, null, false );
		wp_enqueue_script( 'jquery' );

		//Main Theme JS file
		if( 'development' === $env ) {
			
			wp_register_script( 'svg4everybody', get_template_directory_uri() . '/resources/js/svg4everybody.js', null, null, true );
			wp_enqueue_script( 'svg4everybody' );
			
			wp_register_script( 'am2-functions', get_template_directory_uri() . '/resources/js/am2-core.js', null, null, true );
			wp_enqueue_script( 'am2-functions' );
			
			wp_register_script( 'functions', get_template_directory_uri() . '/resources/js/functions.js', null, null, true );
			wp_enqueue_script( 'functions' );
		} else {
			$dirJS = new DirectoryIterator( get_stylesheet_directory() . '/assets/build/' ); // Target JS Build Path
			foreach ($dirJS as $file) {
				if ( pathinfo($file, PATHINFO_EXTENSION) === 'js' ) {
					$fullName = basename($file);
					$name = substr(basename($fullName), 0, strpos(basename($fullName), '.'));
					$deps = array('style');
					$media = 'all';

					wp_register_script( $name, get_stylesheet_directory_uri() . '/assets/build/' . $fullName , null, null, true );
					wp_enqueue_script( $name );
				}
			}
		}

	}
	
}

/*
 *  Hook inline Javascript to head
 */
function am2_hook_javascript() {
    ?>
        <script>
            var am2 = window.am2 || {};
			am2.main = {};
			am2.functionsQueue = [];
			am2.templateDirectory = '<?php echo get_template_directory_uri(); ?>';
        </script>
    <?php
}
add_action( 'wp_head', 'am2_hook_javascript' );

/*
 *  Enable svg images in media uploader
 */
function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/*
 *  Display svg images on media uploader and feature images
 */
function fix_svg_thumb_display() {
	echo '<style> td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { width: 100% !important; height: auto !important; } </style>';
}
add_action('admin_head', 'fix_svg_thumb_display');

/*
 *  Disable WPCF7 styles
 */
if( function_exists( 'wpcf7_load_css' ) ) {
	add_filter( 'wpcf7_load_css', '__return_false' );
}

/*
 *  Add Google Maps API Key to ACF
 */
function am2_acf_google_map_api( $api ) {
	
	$api['key'] = get_field( 'google_maps_api_key', 'options' );
	
	return $api;
	
}
add_filter( 'acf/fields/google_map/api', 'am2_acf_google_map_api' );