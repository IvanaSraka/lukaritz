<?php
/**
 * Class loader using SPL autoloader.
 */
class AM2_Autoloader
{
    /**
     * Registers Twig_Autoloader as an SPL autoloader.
     *
     * @param bool $prepend Whether to prepend the autoloader or not.
     */
    public static function register($prepend = false)
    {

        if (PHP_VERSION_ID < 50300) {
            spl_autoload_register(array(__CLASS__, 'autoload'));
        } else {
            spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
        }
    }

    /**
     * Handles autoloading of classes.
     *
     * @param string $class A class name.
     */
    public static function autoload($class)
    {
    	
        if (0 !== strpos($class, 'AM2_Flexible_Layout')) {
            return;
        }

        //echo dirname(__FILE__) . '/../' . str_replace(array('_', "\0"), array('/', ''), $class) . '.php'; die;

        $filename = str_replace("AM2_Flexible_Layout_Render_","",$class);
        if (is_file($file = dirname(__FILE__).'/'.$filename.'.php')) {
            require $file;
        }
    }
}
/*echo get_include_path() . PATH_SEPARATOR . ABSPATH .'wp-content/themes/stranica/includes/flexible-layout/';
set_include_path( get_include_path() . PATH_SEPARATOR . ABSPATH .'wp-content/themes/stranica/includes/flexible-layout/' );
spl_autoload_extensions('.php');

// Use default autoload implementation
spl_autoload_register();*/

//include('content_numbered_boxs.php');
//include('content_price_boxes.php');