<div class="blog-single__top global-text">
	<?php _e( 'Latest entries', 'am2' ); ?>
</div>
<!-- start:post array -->
<?php 
    $post_type = 'blog';

    $args = array (
        'post_type' => $post_type,
        'order' => 'DESC',
        'posts_per_page' => 5,
         'post__not_in' => array( $post->ID )
    );
    $query = new WP_Query($args); 
?>
<!-- end:post array -->

<?php if( $query->have_posts() ) { ?>
    <div class="blog-single__list">
    <?php while ($query->have_posts()) {  $query->the_post(); ?>                    
      	<figure class="blog-single__list-item" style="background-image:url( <?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?> );"> 
            <meta itemprop="image" content=" <?php echo  the_post_thumbnail_url($query->ID, 'medium'); ?> "> 
            <a href="<?php the_permalink(); ?>" class="aktualno__link">
                <div class="aktualno__article-title">
                    <span class="aktualno__date"><?php the_date($query->ID); ?></span>
                    <h3 class="aktualno__link-title"><?php the_title(); ?></h3>
                </div>
                <svg class="svg-arrow aktualno__arrow">
                    <use xlink:href="<?php bloginfo('template_directory'); ?>/assets/images/sprite.svg#arrow--left-new"></use>
                </svg>
            </a>
        </figure>   
<?php  } ?>
    </div>
<?php  } ?> 
<a href="<?php echo get_bloginfo( 'url' ); ?>/blog/" class="button button--primary button--full" tabindex="0">                       
    <?php _e( 'Blog archive', 'am2' ); ?>            
</a>  