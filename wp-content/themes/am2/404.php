<?php get_header(); ?>

	<!-- start:content -->
	<div id="content" class="page-404">

		<!-- start:main 	-->
	    <main id="main" class="main clearfix">
	    	<!-- start:header title-->
			<?php 
		        $header_title = array();         
		        $header_title_image = get_field( 'main_header_image', 'options' );
		        $header_title['header_image'] = $header_title_image['url'];	       
				$header_title['header_title'] =  __( '404 Page not found', 'am2' ); 
				 
		        Timber::render('header-title.twig', $header_title);            
		    ?>
		    <!-- end:header title-->
	 
			<div class="wrapper layout">
				<div class="container clearfix"> 

					<h2><?php _e( 'Sorry, but the page you were trying to view does not exist.', 'am2' ); ?></h2>
					<p>&nbsp;<br/></p>
					<a href="<?php echo site_url(); ?>" class="button button--primary"><?php _e( 'Go to Homepage', 'am2' ); ?></a>
					<p>&nbsp;<br/></p><p>&nbsp;<br/></p>
				</div>
			</div>

	    </main>
	    <!-- end:main -->

	</div>
	<!-- end:content -->
	
<?php get_footer(); ?>