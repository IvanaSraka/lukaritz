<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/** Environment development/production **/
define('ENV', 'development');

/** Setting max number of revisions per post/page **/
define( 'WP_POST_REVISIONS', 3 );

/** Multisite settings **/
/*
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'example.dev');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
*/
/* Multisite */
/*
define( 'WP_ALLOW_MULTISITE', true );
*/

if(empty($_SERVER['HTTP_HOST'])) {
	$_SERVER['HTTP_HOST'] = DOMAIN_CURRENT_SITE;
}

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lukaritz_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-gu,@^ffuz&sg:)O_&+|Db),+ x/BG4%@kezj1r;!d-<&@qnYvrSCu2pf/go%,Pk');
define('SECURE_AUTH_KEY',  'a}:Q(GaeOd[F-wb^p/Tm*{]@{! 1CzM>;z|oyN_1^c;NiZTcV>jV0%+-I%&Q%^5,');
define('LOGGED_IN_KEY',    'z}_i/10}jR/.f.g&h?-3DYIDSktgZ]-&(i$Sc9V-mH20|Ys|0NMKZ)uDI6L!*[=9');
define('NONCE_KEY',        '`x!w-bx[T-(]oj<H|W+dNUd??@*xFFe2gWV&z,r/~Oq-aD/WBI^<r/]NVA0#5NmL');
define('AUTH_SALT',        'Qv^32/q+mW}8;|d/[[>.J~8rjL0}W>S)<p_QZ]e?Y%F#esZr0lI,Gs=t6=dK0tC#');
define('SECURE_AUTH_SALT', ':UP,;57k+@LCF/bNvP}tM$R|Gm=rAyU{-vx._Y~<[;wJ/1IC(%0|q+d vyc-]@3y');
define('LOGGED_IN_SALT',   '7/S`@dK]2zjwSFlKY~GaysnPOjtRQewu.VJm2P+wbH^JRgqr)p#oy23cz]m~nH9O');
define('NONCE_SALT',       '*5@rEX*]dmYLy1JY+y(cR-_j5v.?E7{vd&$t0m~tSV!4a0]d>7gQz+k|p`-U69&o');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'am2_2017_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
if ( isset( $_GET['wp_debug'] ) && 'on' === $_GET['wp_debug'] ) {
    define( 'WP_DEBUG', true );
    define( 'WP_DEBUG_DISPLAY', true );
} else {
    define( 'WP_DEBUG', false );
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');